"use strict";

require("../helpers/setup");

var wd = require("wd"),
    _ = require('underscore'),
    actions = require("../helpers/actions"),
    serverConfigs = require('../helpers/appium-servers');

wd.addPromiseChainMethod('swipe', actions.swipe);

describe("android simple", function () {
  this.timeout(300000);
  var driver;
  var allPassed = true;

  before(function () {
    var serverConfig = process.env.npm_package_config_sauce ?
      serverConfigs.sauce : serverConfigs.local;
    serverConfig = serverConfigs.local;  //qqq

    driver = wd.promiseChainRemote(serverConfig);
    require("../helpers/logging").configure(driver);

    var desired = process.env.npm_package_config_sauce ?
      _.clone(require("../helpers/caps").android18) :
      _.clone(require("../helpers/caps").abcME);

    desired = _.clone(require("../helpers/caps").abc);

    // desired.app = require("./helpers/apps").androidAppABC;
    // desired.app = require("./helpers/apps").androidApiDemos;
    if (!desired.appPackage)
      desired.app = require("../helpers/apps").androidApiDemos;

    // if (process.env.npm_package_config_sauce) {
    //   desired.name = 'android - simple';
    //   desired.tags = ['sample'];
    // }
    return driver
      .init(desired)
      .setImplicitWaitTimeout(3000);
  });

  after(function () {
    return driver
      .quit()
      // .finally(function () {
      //   if (process.env.npm_package_config_sauce) {
      //     return driver.sauceJobStatus(allPassed);
      //   }
      // });
  });

  afterEach(function () {
    allPassed = allPassed && this.currentTest.state === 'passed';
  });

  it("should find an element", function () {
    return driver
      .elementById('au.net.abc.iview:id/action_search')
        .click()
      .elementById('au.net.abc.iview:id/search_edit_text')
        // .should.eventually.exist
        .type('night')
      .elementByXPath("//android.widget.TextView[contains(@text,'In The Night Garden')]")
        .click()
      .elementByXPath("//android.widget.TextView[contains(@text,'The Tombliboos Swap Trousers')]")
        .click()
      .elementByXPath("//android.widget.TextView[contains(@text,'ABC Kids')]")
        .click().sleep(2000)
        .swipe({ startX: 932, startY: 580, endX: 170,  endY: 580, duration: 800 })
        .sleep(2000)
        .swipe({ startX: 932, startY: 580, endX: 170,  endY: 580, duration: 800 })
        .sleep(2000)


    // console.log('----------- xx ----------- ');
    // console.log(xx);
    // return xx;
    
    
      //   .click()
      // .elementsByAndroidUIAutomator('new UiScrollable(new UiSelector().resourceId("android:id/list")).scrollIntoView(new UiSelector().textContains("hoopla doopla"))')
      //   .click()



      // .back()
      // .elementByName('App')
      //   .should.eventually.exist
      // .elementsByAndroidUIAutomator('new UiSelector().clickable(true)')
      //   .should.eventually.have.length(12)
      // .elementsByAndroidUIAutomator('new UiSelector().enabled(true)')
      //   .should.eventually.have.length.above(20)
      // .elementByXPath('//android.widget.TextView[@text=\'API Demos\']')
      //   .should.exists;
  });
});
