exports.ios92 = {
  browserName: '',
  'appium-version': '1.3',
  platformName: 'iOS',
  platformVersion: '9.2',
  deviceName: 'iPhone 5s',
  app: undefined // will be set later
};

exports.ios81 = {
  browserName: '',
  'appium-version': '1.3',
  platformName: 'iOS',
  platformVersion: '8.1',
  deviceName: 'iPhone Simulator',
  app: undefined // will be set later
};

exports.android18 = {
  browserName: '',
  'appium-version': '1.3',
  platformName: 'Android',
  platformVersion: '4.3',
  deviceName: 'Android Emulator',
  app: undefined // will be set later
};

exports.android19 = {
  browserName: '',
  'appium-version': '1.3',
  platformName: 'Android',
  platformVersion: '4.4.2',
  deviceName: 'Android Emulator',
  app: undefined // will be set later
};

exports.selendroid16 = {
  browserName: '',
  'appium-version': '1.3',
  platformName: 'Android',
  platformVersion: '4.1',
  automationName: 'selendroid',
  deviceName: 'Android Emulator',
  app: undefined // will be set later
};

exports.s5 = {
  browserName: '',
  'appium-version': '1.3',
  platformName: 'Android',
  platformVersion: '6.0.1',
  deviceName: 'Samsung Galaxy 5',
  appPackage: "io.selendroid.testapp",
  appActivity: ".HomeScreenActivity"
  // app: undefined // will be set later
};

exports.abc = {
  browserName: '',
  'appium-version': '1.3',
  platformName: 'Android',
  platformVersion: '6.0.1',
  deviceName: 'Samsung Galaxy 5',
  appPackage: "au.net.abc.iview",
  appActivity: "au.net.abc.iview.activities.LaunchActivity"
  // app: undefined // will be set later
};

exports.abcME = {
  browserName: '',
  'appium-version': '1.3',
  platformName: 'Android',
  platformVersion: '6.0.1',
  deviceName: 'Samsung Galaxy 5',
  appPackage: "au.net.abc.abcme",
  appActivity: "au.net.abc.abcme.ui.activities.HomeActivity"
  // app: undefined // will be set later
};
